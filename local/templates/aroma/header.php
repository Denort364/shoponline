<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?$APPLICATION->ShowTitle()?></title>
  <?$APPLICATION->ShowHead();?>
  <?php
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/bootstrap/bootstrap.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/fontawesome/css/all.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/themify-icons/themify-icons.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/nice-select/nice-select.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/owl-carousel/owl.theme.default.min.css");
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/owl-carousel/owl.carousel.min.css");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/jquery/jquery-3.2.1.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/bootstrap/bootstrap.bundle.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/skrollr.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/owl-carousel/owl.carousel.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/nice-select/jquery.nice-select.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/jquery.ajaxchimp.min.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendors/mail-script.js");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");
  ?>
  <link rel='icon' href="<?=SITE_TEMPLATE_PATH?>/img/Fevicon.png" type='image/png'>
  
</head>
<body>
  <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
  <!--================ Start Header Menu Area =================-->
	<header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <a class="navbar-brand logo_h" href="index.html"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "",
            Array(
              "ALLOW_MULTI_SELECT" => "N",
              "CHILD_MENU_TYPE" => "left",
              "COMPOSITE_FRAME_MODE" => "A",
              "COMPOSITE_FRAME_TYPE" => "AUTO",
              "DELAY" => "N",
              "MAX_LEVEL" => "1",
              "MENU_CACHE_GET_VARS" => array(""),
              "MENU_CACHE_TIME" => "3600",
              "MENU_CACHE_TYPE" => "N",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "ROOT_MENU_TYPE" => "main",
              "USE_EXT" => "N"
            )
          );?>

            <ul class="nav-shop">
              <li class="nav-item"><button><i class="ti-search"></i></button></li>
              <li class="nav-item"><button><i class="ti-shopping-cart"></i><span class="nav-shop__circle">3</span></button> </li>
              <li class="nav-item"><a class="button button-header" href="#">Buy Now</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>