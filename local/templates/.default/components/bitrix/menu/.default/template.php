<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="nav navbar-nav menu_nav ml-auto mr-auto" style="padding-left: 50px;">

<?
foreach($arResult as $arItem):
    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
        continue;
?>
    <?if($arItem["SELECTED"]):?>
        <li><a href="<?=$arItem["LINK"]?>" class="nav-item active" style="margin-right: 40px;"><?=$arItem["TEXT"]?></a></li>
    <?else:?>
        <li><a href="<?=$arItem["LINK"]?>" class="nav-item" style="margin-right: 40px;"><?=$arItem["TEXT"]?></a></li>
    <?endif?>
    
<?endforeach?>

</ul>
<?endif?>