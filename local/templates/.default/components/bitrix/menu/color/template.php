<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<form action="#">
    <ul>
        <?foreach($arResult as $arItem):?>
        <li class="filter-list"><input class="pixel-radio" type="radio" name="color"><label for="apple"><?=$arItem["TEXT"]?></label></li>
        <?endforeach?>
<?endif?>